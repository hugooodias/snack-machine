
# Snack machine

A microservice to purchase snacks from minutrade snack machine

## [Demo](https://snack-machine.herokuapp.com/api-docs/)

## Table of Contents

- [Technology](#technology)
- [Developing](#developing)
	- [First Install](#first-install)
	- [Running tests](#running-the-server)
  - [Seed](#seed)
- [Using the API](#using-the-api)

## Technology

This app uses the following technologies:

- **[Docker](https://docs.docker.com)** and **[Docker Compose](https://docs.docker.com/compose/)** to create the development environment.
- **[MongoDB](https://www.mongodb.com)** as to store the data and **[Mongoose](http://mongoosejs.com)** as a Node.js ORM.
- **[Swagger](https://swagger.io)** as API documentation system.
- **[Express](https://github.com/expressjs/express)** as a tool to build the web server that handles the API endpoints.

## Developing

In order to develop this project you must have [Docker](https://docs.docker.com/)
and [Docker Compose](https://docs.docker.com/compose/) installed.

### First Install

If you never developed in this repo before:

1. **Clone the repository:**
```
$ git clone git@bitbucket.org:hugooodias/snack-machine.git
```

2. **Run the server for the first time:**
```
$ cd snack-machine
$ docker-compose build
```

### Running the server

1. **Start the server**
```
$ docker-compose up
```

### Seed
There is a seed file that populate the database with 3 cards and 5 items.

2. **Run the seed**
```
$ docker-compose run --rm web node app/seed.js
```

## Using the api

The api docs uses swagger for its documentation and tests.

After start the server access [localhost:3030/api-docs](https://localhost:3030/api-docs)

### Useful endpoints

#### Checking cards balance
`GET /cards/{cardId}` will retrieve the card balance

#### Listing available items
`GET /items` will retrieve all items in the system

#### Purchasing an item on a card
`POST /cards/{cardId}/transactions/{itemId}` Will purchase an item if the card has enough balance.

You can check the entire list here: [snack-machine.herokuapp.com/api-docs](https://snack-machine.herokuapp.com/api-docs/#/)
