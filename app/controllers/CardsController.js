const Card = require("../models/Card");

const List = async (req, res) => {
  try {
    const cards = await Card.listCards();
    res.statusCode = 200;
    res.json({ cards });
  } catch (e) {
    res.statusCode = 500;
    res.json({ error: "Error on getting card list" });
  }
};

const Show = async (req, res) => {
  try {
    const card = await Card.getCard(req.params.cardId);
    res.statusCode = 200;
    res.json({ card });
  } catch (e) {
    res.statusCode = 404;
    res.json({ error: e });
  }
};

const Create = async (req, res) => {
  try {
    const card = await Card.createCard();
    res.statusCode = 200;
    res.json({ card });
  } catch (e) {
    res.statusCode = 500;
    res.json({ error: "Error on creating the card" });
  }
};

module.exports = { List, Show, Create };
