const Item = require("../models/Item");

const Index = async (req, res) => {
  try {
    const items = await Item.listItems();

    res.status = 200;
    res.json({ items });
  } catch (e) {
    res.statusCode = 500;
    res.json({ error: "Error on listing items" });
  }
};

const Create = async (req, res) => {
  try {
    const availablePrices = [2, 5.5];
    const itemValue =
      availablePrices[Math.floor(Math.random() * availablePrices.length)];
    const item = await Item.createItem("This is an item", itemValue);

    res.status = 200;
    res.json({ item });
  } catch (e) {
    res.statusCode = 500;
    res.json({ error: "Error on creating an item" });
  }
};

module.exports = { Index, Create };
