const Card = require("../models/Card");
const Item = require("../models/Item");

const Create = async (req, res) => {
  try {
    const card = await Card.getCard(req.params.cardId);
    const item = await Item.getItem(req.params.itemId);

    if (insuficientFounds(card, item)) {
      throw new InsuficientFoundsException(
        `The card ${card._id} does not have found to purchase the item ${
          item.name
        }`
      );
    }

    await Card.addTransaction(req.params.cardId, -Math.abs(item.value), item);

    res.statusCode = 200;
    res.json({ message: "Item purchased successefully" });
  } catch (e) {
    res.statusCode = 403;
    res.json({ error: e.message });
  }
};

const insuficientFounds = (card, item) => {
  return parseFloat(item.value) > parseFloat(card.balance);
};

function InsuficientFoundsException(message) {
  this.message = message;
  this.name = "InsuficientFoundsException";
}

module.exports = { Create };
