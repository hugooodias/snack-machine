const DAILY_CREDIT_VALUE = 5.5;
const mongoose = require("mongoose");
const { transactionSchema } = require("../Transaction");

const cardSchema = new mongoose.Schema({
  name: "string",
  transactions: [transactionSchema]
});

const Card = mongoose.model("Card", cardSchema);
const ObjectId = require("mongoose").Types.ObjectId;

const getCard = async cardId => {
  try {
    await addDailyCredit(cardId);
    const results = await getBalance(cardId);

    if (!results.length) throw "Card not found";

    return results[0];
  } catch (e) {
    throw e;
  }
};

const addDailyCredit = async cardId => {
  const beginningOfDay = new Date();
  beginningOfDay.setHours(0, 0, 0, 0);

  return Card.find(
    {
      transactions: {
        $elemMatch: { createdAt: { $gte: beginningOfDay } }
      },
      _id: ObjectId(cardId)
    },
    { "transactions.$": 1 }
  )
    .exec()
    .then(cards => {
      if (!cards.length) {
        return addTransaction(cardId, DAILY_CREDIT_VALUE);
      }
      return;
    })
    .catch(err => {
      return "error occured";
    });
};

const addTransaction = async (cardId, amount, item) => {
  return Card.updateOne(
    {
      _id: ObjectId(cardId)
    },
    {
      $push: {
        transactions: {
          amount: amount,
          item: item,
          createdAt: new Date()
        }
      }
    }
  )
    .exec()
    .catch(error => console.log(error));
};

const createCard = () => {
  return Card.create({
    name: "This is a card"
  });
};

const listCards = () => {
  return Card.find()
    .exec()
    .then(cards => {
      return cards;
    })
    .catch(err => {
      return "error occured";
    });
};

const getBalance = cardId => {
  const beginningOfDay = new Date();
  beginningOfDay.setHours(0, 0, 0, 0);

  return Card.aggregate(
    [
      {
        $match: {
          _id: ObjectId(cardId),
          transactions: {
            $elemMatch: { createdAt: { $gte: beginningOfDay } }
          }
        }
      },
      { $unwind: "$transactions" },
      {
        $group: {
          _id: "$_id",
          balance: { $sum: "$transactions.amount" }
        }
      }
    ],
    (err, result) => {
      if (err) {
        console.log(err);
        return;
      }
      return result;
    }
  );
};

module.exports = {
  addTransaction,
  getCard,
  createCard,
  listCards
};
