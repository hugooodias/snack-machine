const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({ name: "string", value: "number" });
const Item = mongoose.model("Item", itemSchema);
const ObjectId = require("mongoose").Types.ObjectId;

const createItem = (name, value) => {
  return Item.create({
    name: name,
    value: parseFloat(value)
  });
};

const getItem = itemId => {
  return Item.findOne({ _id: ObjectId(itemId) }).exec();
};

const listItems = () => {
  return Item.find()
    .exec()
    .then(items => {
      return items;
    })
    .catch(err => {
      return "error occured";
    });
};

module.exports = {
  itemSchema,
  Item,
  getItem,
  createItem,
  listItems
};
