const mongoose = require("mongoose");
const { itemSchema } = require("../Item");

const transactionSchema = new mongoose.Schema({
  kind: "string",
  amount: "number",
  item: itemSchema,
  createdAt: { type: Date, default: Date.now }
});

module.exports = {
  transactionSchema
};
