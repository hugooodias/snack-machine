const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const CardsController = require("../controllers/CardsController");
const TransactionsController = require("../controllers/TransactionsController");
const ItemsController = require("../controllers/ItemsController");

router.get("/", function(req, res, next) {
  res.statusCode = 200;
  res.json({
    message: "Welcome! Visit /api-docs to check available endpoints"
  });
});

router.get("/cards", CardsController.List);
router.get("/cards/:cardId", CardsController.Show);
router.post("/cards", CardsController.Create);

router.get("/items", ItemsController.Index);
router.post("/items", ItemsController.Create);

router.post(
  "/cards/:cardId/transactions/:itemId",
  TransactionsController.Create
);

module.exports = router;
