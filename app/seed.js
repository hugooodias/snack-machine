var seeder = require("mongoose-seed");

// Connect to MongoDB via Mongoose
seeder.connect(
  "mongodb://mongo/test",
  function() {
    // Load Mongoose models
    seeder.loadModels(["app/models/Card", "app/models/Item"]);

    // Clear specified collections
    seeder.clearModels(["Card", "Item"], function() {
      // Callback to populate DB once collections have been cleared
      seeder.populateModels(data, function() {
        seeder.disconnect();
      });
    });
  }
);

// Data array containing seed data - documents organized by Model
var data = [
  {
    model: "Card",
    documents: [
      {
        name: "Card 1"
      },
      {
        name: "Card 2"
      },
      {
        name: "Card 3"
      }
    ]
  },
  {
    model: "Item",
    documents: [
      {
        name: "Coca-cola",
        value: 5.5
      },
      {
        name: "Ruffles",
        value: 2
      },
      {
        name: "Quacker",
        value: 2
      },
      {
        name: "Sprite",
        value: 5.5
      },
      {
        name: "Toddy",
        value: 2
      }
    ]
  }
];
